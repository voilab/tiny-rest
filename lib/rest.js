/*jslint browser: true */
/*global define */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['angular'], factory);
    } else {
        // Browser globals
        root.amdWeb = factory(root.angular);
    }
}(this, function (angular) {

    angular.module('rest', []).provider('Rest', function () {
        var
            /**
             *  URL de base de l'API REST ciblée.
             *
             *  @type {String}
             */
            api_url = '',

            /**
             * Service de création des models.
             *
             * @type {Object}
             */
            ModelService = {
                create: function (undef, base) {
                    return base;
                }
            };

        /**
         *  Définition du chemin de base de l'API REST.
         *
         *  @param {String} api_url Lien de base de l'API.
         *  @return {this}
         */
        this.setApiUrl = function (new_api_url) {
            api_url = /\/$/.test(new_api_url) ?
                new_api_url.substring(0, new_api_url.length - 1) :
                new_api_url;

            return this;
        };

        /**
         * Défini le service de création des modles.
         *
         * @param {Object} service
         *  @return {this}
         */
        this.setModelService = function (service) {
            if (typeof service.create === 'function') {
                ModelService = service;
            }

            if (typeof service.$get === 'function') {
                ModelService = new service.$get();
            }

            return this;
        };

        this.$get = ['$http', '$q', function ($http, $q) {
            var
                /**
                 * Valeurs par défaut pour une instance Rest
                 *
                 * @type {Object}
                 */
                default_config = {
                    /**
                     *  Nom de la resource REST
                     *
                     *  @type {String}
                     */
                    resource: '',

                    /**
                     *  Objet du model à utliliser lors de la création de l'objet de résultat
                     *
                     *  @type {Object}
                     */
                    model: function (props) {
                        angular.extend(this, props);
                    }
                },

                /**
                 *  Création d'un objet de requête sur une resourcce de l'API REST.
                 *
                 *  @param {String} resource Nom de la resource
                 *  @param {Object} {model}    Objet de base pour la création des objets de résultats.
                 */
                Rest = function (resource, model) {
                    angular.extend(this, default_config);

                    if (resource) {
                        this.setResource(resource);
                    }

                    if (model) {
                        this.setModel(model);
                    }
                };

            Rest.prototype = {
                /**
                 *  Injection de du model dans le ou les objets reçus de l'API
                 *
                 *  @param  {$q.defer} promise        Promesse
                 *  @param  {String} collection_type Type de collection reçue
                 *
                 *  @return {$q.defer}                 Promesse
                 */
                injectModel: function (promise) {
                    var defered = $q.defer(),
                        that = this;

                    promise.then(
                        function (result) {
                            if (result) {
                                defered.resolve(
                                    ModelService.create(that.model, result)
                                );
                            } else {
                                // si le résultat est vide, on n'injecte pas de modèle...
                                defered.resolve();
                            }
                        },
                        function () {
                            defered.reject.apply(promise, arguments);
                        }
                    );

                    return defered.promise;
                },

                /**
                 *  Choix de la resource REST
                 *
                 *  @param {String} resource Nom de la resource
                 */
                setResource: function (resource) {
                    this.resource = resource || '';
                },

                /**
                 *  Défini l'objet à utiliser comme model de base
                 *
                 *  @param {Object} model
                 */
                setModel: function (model) {
                    this.model = model || function (base) {
                            angular.extend(this, base);
                        };
                },

                /**
                 *  Uri de base de la resource, sans aucun paramètre
                 *
                 *  @return {String}
                 */
                getResourceBaseUri: function () {
                    return api_url + '/' + this.resource;
                },

                /**
                 *  Uri de la resource avec les paramètre passés
                 *
                 *  @param  {String || Array} args Arguments de la resource
                 *
                 *  @return {String}
                 */
                getResourceUri: function (args) {
                    var path = this.getResourceBaseUri();

                    if (!args) {
                        return path;
                    }

                    if (args instanceof Array) {
                        args = args.join('/');
                    }

                    path = path + '/' + args;
                    path = path.replace('/?', '?');

                    return path;
                },

                /**
                 *  Requête sur l'API
                 *
                 *  @param  {String} method Méthode de requête
                 *  @param  {String || Array} path   Arguments de la requête
                 *  @param  {Object} data Les données postées (s'il y en a)
                 *
                 *  @return {$q.defer}        Promesse
                 */
                request: function (method, path, data) {
                    var defered = $q.defer();

                    if (angular.isObject(path) && data === undefined) {
                        data = path;
                        path = '';
                    }

                    $http({
                        method: method,
                        url: this.getResourceUri(path),
                        data: data
                    }).success(function (data, status, headers) {
                        defered.resolve(data, status, headers);
                    }).error(function (data, status, headers) {
                        defered.reject(data, status, headers);
                    });

                    return defered.promise;
                },

                /**
                 *  Requête GET sur l'API
                 *
                 *  @param  {String || Array} path Arguments de la resource
                 *
                 *  @return {$q.defer}      Promesse
                 */
                get: function (path) {
                    return this.injectModel(
                        this.request('GET', path)
                    );
                },

                /**
                 *  Requête GET sur l'API, sans l'injection de modèle
                 *
                 *  @param  {String || Array} path Arguments de la resource
                 *
                 *  @return {$q.defer}      Promesse
                 */
                getRaw: function (path) {
                    return this.request('GET', path);
                },

                /**
                 *  Requête PUT sur l'API
                 *
                 *  @param  {String || Array} path Arguments de la resource
                 *  @param  {Object} data Les données à envoyer à l'api
                 *
                 *  @return {$q.defer}      Promesse
                 */
                put: function (path, data) {
                    return this.request('PUT', path, data);
                },

                /**
                 *  Requête POST sur l'API
                 *
                 *  @param  {String || Array} path Arguments de la resource
                 *  @param  {Object} data Les données à envoyer à l'api
                 *
                 *  @return {$q.defer}      Promesse
                 */
                post: function (path, data) {
                    return this.request('POST', path, data);
                },

                /**
                 *  Requête DELETE sur l'API
                 *
                 *  @param  {String || Array} path Arguments de la resource
                 *
                 *  @return {$q.defer}      Promesse
                 */
                del: function (path) {
                    return this.request('DELETE', path);
                },

                /**
                 *  Liste des éléments de cette resource
                 *
                 *  @return {$q.defer} Promesse
                 */
                getList: function () {
                    return this.injectModel(
                        this.get()
                    );
                },

                /**
                 *  Lecture d'un élément de cette resource
                 *
                 *  @param  {String || Array} identifier Id de la resource ou tableau d'arguments
                 *
                 *  @return {$q.defer} Promesse
                 */
                getOne: function (identifier) {
                    return this.injectModel(
                        this.get(identifier)
                    );
                },

                /**
                 *  Mise à jour d'un élément de cette resource
                 *
                 *  @param  {String || Array} identifier Id de la resource ou tableau d'arguments
                 *  @param  {Object}          data       Données à envoyer
                 *
                 *  @return {$q.defer} Promesse
                 */
                update: function (identifier, data) {
                    return this.put(identifier, data);
                },

                /**
                 *  Supression d'un élément de cette resource
                 *
                 *  @param  {String || Array} identifier Id de la resource ou tableau d'arguments
                 *
                 *  @return {$q.defer} Promesse
                 */
                remove: function (identifier) {
                    return this.del(identifier);
                },

                /**
                 *  Ajout d'un élément de cette resource
                 *
                 *  @param  {Object}   data       Données à envoyer
                 *
                 *  @return {$q.defer} Promesse
                 */
                add: function (data) {
                    return this.post(data);
                }
            };

            return Rest;
        }];
    });
}));
